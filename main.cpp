//1. 
//2. 
//3. 
//4. 
//5. 
//6. 
//7. 
//8.	 
//		
//		
//		
//		�������� �����������: ���� ����� ��������� �� ���������� � ���� ���� �� ���� ���������� � �� ����� �� 'F' - �� ���������� ����� � ������ � 1�� �����.
//		�� ����� ������ ������ ��������� �������. 
//9. 

#include <iostream>
#include <Windows.h>
#include <conio.h>

using namespace std;

const size_t col = 20u;
const size_t row = 20u;

char map[row][col] = {
	"###################",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"###################"
};

enum Color
{
	black,
	blue,
	green,
	cyan,
	red,
	violet,
	yellow,
	white,
	grey,
	lightBlue,
	lightGreen,
	brightCyan,
	pink,
	brightViolet,
	lightYellow,
	brightWhite
};

struct Position
{
	decltype(COORD::X) x;
	decltype(COORD::Y) y;
};

struct Player
{
	Position position;
	int HP;
};

struct Bombs
{
	int amount;
};

struct Heals
{
	int amount;
};

struct Victory
{
	Position position;
	int color;
};

struct Explosive 
{
	int amount;
};

struct ControlKeys
{
	char up;
	char down;
	char right;
	char left;
	char space_key;
	char explosion;
};

struct ObjectColors
{
	Color bomb;
	Color heal;
	Color wall;
	Color victory;
	Color player;
	Color standart;
	Color explosive;
};

struct ObjectChars
{
	char bomb;
	char heal;
	char wall;
	char victory;
	char player;
	char space;
	char explosive;
};

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
struct ObjectData
{
	ObjectChars chars;
	ObjectColors colors;
};

//Declarations
void generateBombs(const Bombs& bomb, const ObjectChars& ObjectChars);
void generateHeals(const Heals& heal, const ObjectChars& ObjectChars);
void generateVictory(const Victory& victory, const ObjectChars& ObjectChars);
void setCursorColor(Color color);
Color getCharColor(const ObjectColors& objectColors, const ObjectChars& ObjectChars, char ch);
void printMap(const ObjectColors& objectColors, const ObjectChars& ObjectChars);
void setCursorPosition(SHORT col, SHORT row);
void showConsoleCursor(bool showFlag = true);
void setCursorPosition(Position position);
Position playerControl(Player& player, const ControlKeys& controlKeys, const ObjectChars& ObjectChars, const ObjectColors& objectColors);
void drawPlayer(const Player& player, const ObjectColors& objectColors, const ObjectChars& ObjectChars);
void smove(Player& player, const ControlKeys& controlKeys, const ObjectChars& ObjectChars, const ObjectColors& objectColors);
void initialization(Bombs& bomb, Heals& heal, const Victory& victory, Explosive& explosive, const ObjectColors& objectColors, const ObjectChars& ObjectChars);
int generate�oordinatesForPlayer();
void generateExplosive(const Explosive& explosive, const ObjectChars& ObjectChars);
void explosionFieldCheck(Player& player, const ObjectChars& ObjectChars, const ObjectColors& objectColors);
void boom(int a, int b, const ObjectChars& ObjectChars, const ObjectColors& objectColors);

int main()
{
	srand(time(nullptr));
	int coordinate = generate�oordinatesForPlayer();

	Player player = { { coordinate, coordinate }, 20 };
	Bombs bomb = { 17 };
	Heals heal = { 2 };
	Explosive explosive = { 1 };
	int cordX = 1 + rand() % (col - 2);
	Victory victory = { { cordX, (row - 1) }, blue };
	ObjectColors objectColors = { red, green, yellow, blue, brightWhite, brightWhite, lightBlue };
	ControlKeys controlKeys = { 'w', 's', 'd', 'a', ' ' ,'f' };
	ObjectChars ObjectChars = { '%', '&', '#', '$', '@', ' ', '^' };

	initialization(bomb, heal, victory, explosive, objectColors, ObjectChars);

	while (true)
	{
		drawPlayer(player, objectColors, ObjectChars);
		smove(player, controlKeys, ObjectChars, objectColors);

		setCursorPosition({ 0, 20 });
		cout << "                        " << endl;
		setCursorPosition({ 0, 20 });
		cout << "Health: " << player.HP << endl;
	}

	return 0;
}

//Implementations
int generate�oordinatesForPlayer() {
	return 1 + rand() % (row - 2);
}

void setCursorColor(Color color)
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(handle, color);
}

Color getCharColor(const ObjectColors& objectColors, const ObjectChars& ObjectChars, char ch)
{
	if (ch == ObjectChars.bomb)
		return objectColors.bomb;
	if (ch == ObjectChars.heal)
		return objectColors.heal;
	if (ch == ObjectChars.victory)
		return objectColors.victory;
	if (ch == ObjectChars.wall)
		return objectColors.wall;
	if (ch == ObjectChars.explosive)
		return objectColors.explosive;

	return objectColors.standart;
}

void printMap(const ObjectColors& objectColors, const ObjectChars& ObjectChars)
{
	for (size_t i = 0; i < row; i++)
	{
		for (size_t j = 0; j < col; j++)
		{
			setCursorColor(getCharColor(objectColors, ObjectChars, map[i][j]));
			cout << map[i][j];
			setCursorColor(objectColors.standart);
		}
		cout << endl;
	}
}

void setCursorPosition(SHORT col, SHORT row)
{
	COORD coord = { col, row };
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

void showConsoleCursor(bool showFlag)
{
	HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_CURSOR_INFO cursorInfo;
	GetConsoleCursorInfo(out, &cursorInfo);
	cursorInfo.bVisible = showFlag;
	SetConsoleCursorInfo(out, &cursorInfo);
}

void setCursorPosition(Position position)
{
	setCursorPosition(position.x, position.y);
}

void generateBombs(const Bombs& bomb, const ObjectChars& ObjectChars)
{
	for (int i = 0; i < bomb.amount; i++)
	{
		map[1 + rand() % (row - 3)][1 + rand() % (col - 3)] = ObjectChars.bomb;
	}
}

void generateHeals(const Heals& heal, const ObjectChars& ObjectChars)
{
	for (int i = 0; i < heal.amount; i++)
	{
		map[1 + rand() % (row - 3)][1 + rand() % (col - 3)] = ObjectChars.heal;
	}
}

void generateVictory(const Victory& victory, const ObjectChars& ObjectChars)
{
	map[victory.position.y][victory.position.x] = ObjectChars.victory;
}

void generateExplosive(const Explosive& explosive, const ObjectChars& ObjectChars)
{
	for (int i = 0; i < explosive.amount; i++)
	{
		map[1 + rand() % (row - 3)][1 + rand() % (col - 3)] = ObjectChars.explosive;
	}
}

//Position playerControl(Player& player, const ControlKeys& controlKeys, const ObjectChars& ObjectChars)
//{
//	auto oldPosition = player.position;
//	auto key = _getch();
//	if (key == controlKeys.right)
//		player.position.x++;
//	if (key == controlKeys.left)
//		player.position.x--;
//	if (key == controlKeys.up)
//		player.position.y--;
//	if (key == controlKeys.down)
//		player.position.y++;
//	if (key == controlKeys.space_key) {
//		int x = player.position.x;
//		int y = player.position.y;
//		//map[x + 1][y] = ObjectChars.wall;
//		map[1][1] = ObjectChars.wall;
//	}	
//
//	return oldPosition;
//}

//Position playerControl(Player& player, const ControlKeys& controlKeys, const ObjectChars& ObjectChars, const ObjectColors& objectColors)
//{
//	auto oldPosition = player.position;
//	auto key = _getch();
//	if (key == controlKeys.right)
//		player.position.x++;
//	if (key == controlKeys.left)
//		player.position.x--;
//	if (key == controlKeys.up)
//		player.position.y--;
//	if (key == controlKeys.down)
//		player.position.y++;
//	if (key == controlKeys.space_key) {
//		int x = player.position.x;
//		int y = player.position.y;
//		if (map[y][x + 1] != ObjectChars.explosive) {
//			map[y][x + 1] = ObjectChars.wall;
//			system("cls");
//			printMap(objectColors, ObjectChars);
//		}
//	}
//
//	return oldPosition;
//}

void boom(int x, int y, const ObjectChars& ObjectChars, const ObjectColors& objectColors) {
	//map[b][a] = ObjectChars.heal;
	//map[b - 1][a] = ObjectChars.heal;
	//map[b + 1][a] = ObjectChars.heal;
	//map[b][a + 1] = ObjectChars.heal;
	//map[b][a - 1] = ObjectChars.heal;
	//map[b - 1][a + 1] = ObjectChars.heal;
	//map[b - 1][a - 1] = ObjectChars.heal;
	//map[b + 1][a + 1] = ObjectChars.heal;
	//map[b + 1][a - 1] = ObjectChars.heal;
	// 5 5
	// 4 4 -> 6 6
	int radius = 11;
	int max_col = x + radius > col - 1 ? col - 1 : x + radius;
	int min_col = (x - radius < 0) ? 0 : x - radius;
	int max_row = y + radius > row - 1 ? row - 1 : y + radius;
	int min_row = (y - radius < 0) ? 0 : y - radius;

	for (size_t i = min_row; i <= max_row; i++)
	{
		for (size_t j = min_col; j < max_col; j++)
		{	
			if (map[i][j] != ObjectChars.wall) {
				map[i][j] = ObjectChars.heal;
				setCursorPosition(j, i);
				setCursorColor(getCharColor(objectColors, ObjectChars, map[i][j]));
				cout << map[i][j];
				setCursorColor(objectColors.standart);
			}	
		}
	}

}

void explosionFieldCheck(Player& player, const ObjectChars& ObjectChars, int x, int y, const ObjectColors& objectColors) {
	if (map[y - 2][x + 2] == ObjectChars.explosive) {
		boom( x + 2, y - 2, ObjectChars, objectColors);
	}
	else if (map[y - 2][x + 1] == ObjectChars.explosive) {
		int b = y - 2; int a = x + 1;
		boom(a, b, ObjectChars, objectColors);
	}
	else if (map[y - 2][x] == ObjectChars.explosive) {
		int b = y - 2; int a = x;
		boom(a, b, ObjectChars, objectColors);
	}
	else if (map[y - 2][x - 1] == ObjectChars.explosive) {
		int b = y - 2; int a = x - 1;
		boom(a, b, ObjectChars, objectColors);
	}
	else if (map[y - 2][x - 2] == ObjectChars.explosive) {
		int b = y - 2; int a = x - 2;
		boom(a, b, ObjectChars, objectColors);
	}
	else if (map[y - 1][x - 2] == ObjectChars.explosive) {
		int b = y - 1; int a = x - 2;
		boom(a, b, ObjectChars, objectColors);
	}
	else if (map[y][x + 2] == ObjectChars.explosive) {
		int b = y; int a = x + 2;
		boom(a, b, ObjectChars, objectColors);
	}
	else if (map[y + 1][x - 2] == ObjectChars.explosive) {
		int b = y + 1; int a = x - 2;
		boom(a, b, ObjectChars, objectColors);
	}
	else if (map[y + 2][x - 2] == ObjectChars.explosive) {
		int b = y + 2; int a = x - 2;
		boom(a, b, ObjectChars, objectColors);
	}
	else if (map[y + 2][x - 1] == ObjectChars.explosive) {
		int b = y + 2; int a = x - 1;
		boom(a, b, ObjectChars, objectColors);
	}
	else if (map[y + 2][x] == ObjectChars.explosive) {
		int b = y + 2; int a = x;
		boom(a, b, ObjectChars, objectColors);
	}
	else if (map[y + 2][x + 1] == ObjectChars.explosive) {
		int b = y + 2; int a = x + 1;
		boom(a, b, ObjectChars, objectColors);
	}
	else if (map[y + 2][x + 2] == ObjectChars.explosive) {
		int b = y + 2; int a = x + 2;
		boom(a, b, ObjectChars, objectColors);
	}
	else if (map[y + 1][x + 2] == ObjectChars.explosive) {
		int b = y + 1; int a = x + 2;
		boom(a, b, ObjectChars, objectColors);
	}
	else if (map[y][x + 2] == ObjectChars.explosive) {
		int b = y; int a = x + 2;
		boom(a, b, ObjectChars, objectColors);
	}
	else if (map[y - 1][x + 2] == ObjectChars.explosive) {
		int b = y - 1; int a = x + 2;
		boom(a, b, ObjectChars, objectColors);
	}
	else if (map[y][x - 2] == ObjectChars.explosive) {
		int b = y; int a = x - 2;
		boom(a, b, ObjectChars, objectColors);
	}

}

Position playerControl(Player& player, const ControlKeys& controlKeys, const ObjectChars& ObjectChars, const ObjectColors& objectColors)
{
	auto oldPosition = player.position;
	auto key = _getch();
	if (key == controlKeys.right)
		player.position.x++;
	if (key == controlKeys.left)
		player.position.x--;
	if (key == controlKeys.up)
		player.position.y--;
	if (key == controlKeys.down)
		player.position.y++;
	if (key == controlKeys.space_key) {
		int x = player.position.x;
		int y = player.position.y;

		if (map[y][x + 1] != ObjectChars.explosive) {
			map[y][x + 1] = ObjectChars.wall;
							
			setCursorColor(getCharColor(objectColors, ObjectChars, map[y][x + 1]));
			setCursorPosition(x + 1, y);

			cout << '#';
			setCursorColor(objectColors.standart);
		}
	}
	if (key == controlKeys.explosion) {
		int x = player.position.x;
		int y = player.position.y;

		explosionFieldCheck(player, ObjectChars , x, y , objectColors);
	}

	return oldPosition;
}

void drawPlayer(const Player& player, const ObjectColors& objectColors, const ObjectChars& ObjectChars)
{
	setCursorPosition(player.position);
	setCursorColor(objectColors.player);
	cout << ObjectChars.player;
	setCursorColor(objectColors.standart);
}

void smove(Player& player, const ControlKeys& controlKeys, const ObjectChars& ObjectChars, const ObjectColors& objectColors) // add colors argument
{
	auto oldPosition = playerControl(player, controlKeys, ObjectChars, objectColors); // add colors
	setCursorPosition(oldPosition);
	cout << ' ';

	if (map[player.position.y][player.position.x] == ObjectChars.bomb)
		player.HP -= 2 + rand() % 4; 
	else if (map[player.position.y][player.position.x] == ObjectChars.heal)
		player.HP += 1 + rand() % 3;
	else if (map[player.position.y][player.position.x] != ObjectChars.space)
		player.position = oldPosition;
	else if (map[player.position.y][player.position.x] == ObjectChars.explosive)
		player.position = oldPosition;
}

void initialization(Bombs& bomb, Heals& heal, const Victory& victory, Explosive& explosive, const ObjectColors& objectColors, const ObjectChars& ObjectChars)
{
	showConsoleCursor(false);
	generateBombs(bomb, ObjectChars);
	generateHeals(heal, ObjectChars);
	generateVictory(victory, ObjectChars);
	generateExplosive(explosive, ObjectChars);
	printMap(objectColors, ObjectChars);
}